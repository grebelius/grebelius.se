import React from 'react';
import PropTypes from 'prop-types';
import { Image, Transformation } from 'cloudinary-react';

const ImageOfABookPage = ({ volume, page }) => (
  <Image cloudName="grebelius" publicId={`one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-${volume}.pdf`}>
    <Transformation
      page={page}
      responsive
      dpr="auto"
      quality="auto"
      width="640"
      crop="scale"
      fetchFormat="auto"
      effect="sharpen:100"
    />
  </Image>
);

ImageOfABookPage.propTypes = {
  volume: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
};

export default ImageOfABookPage;
