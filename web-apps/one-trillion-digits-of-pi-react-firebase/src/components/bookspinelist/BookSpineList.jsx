import React, { Component } from "react";
import ReactList from "react-list";
import { Link } from "react-router-dom";

import Book from "./Book";

import "./css/BookListHeader.css";

import BOUGHT_BOOKS from "../../constants/boughtBooks";

const renderPiBookSpines = index => (
  <Link key={index + 1} className="bookSpineLink" to={`/volume/${index + 1}`}>
    <Book volume={index + 1} bookIsBought={BOUGHT_BOOKS.includes(index + 1)} />
  </Link>
);

class BookSpineList extends Component {
  state = {
    firstVisibleVolume: 1,
    lastVisibleVolume: 1000000
  };

  // componentDidMount() {
  //   window.addEventListener('scroll', this.handleScroll);
  // }

  // componentWillUnmount() {
  //   window.removeEventListener('scroll', this.handleScroll);
  // }

  // handleScroll = () => {
  //   // const { firstVisibleVolume } = this.state;

  //   this.setState({ firstVisibleVolume: this.test.getVisibleRange() });
  // };

  render() {
    const { firstVisibleVolume, lastVisibleVolume } = this.state;
    return (
      <div>
        <div className="bookListHeader">
          <h2>
            Volumes {parseInt(firstVisibleVolume, 10).toLocaleString("en")} to{" "}
            {parseInt(lastVisibleVolume, 10).toLocaleString("en")}
          </h2>
        </div>
        <ReactList
          ref={this.test}
          itemRenderer={renderPiBookSpines}
          length={1000000}
          type="uniform"
          useTranslate3d
        />
      </div>
    );
  }
}

export default BookSpineList;
