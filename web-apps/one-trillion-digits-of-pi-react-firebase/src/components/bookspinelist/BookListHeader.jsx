import React, { Component } from 'react';

import './css/BookListHeader.css';

class BookListHeader extends Component {
  state = {
    firstVisibleVolume: 1,
    lastVisibleVolume: 1000000,
  };

  render() {
    const { firstVisibleVolume, lastVisibleVolume } = this.state;
    return (
      <div className="bookListHeader">
        <h2>
          Volumes {parseInt(firstVisibleVolume, 10).toLocaleString('en')} to{' '}
          {parseInt(lastVisibleVolume, 10).toLocaleString('en')}
        </h2>
      </div>
    );
  }
}

export default BookListHeader;
