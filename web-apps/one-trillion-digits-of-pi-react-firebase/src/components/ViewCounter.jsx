import React, { Component } from "react";
import PropTypes from "prop-types";

import db from "../firebase";

class ViewCounter extends Component {
  state = {
    viewcount: 1
  };

  componentDidMount() {
    const { volume } = this.props;
    const defaultDatabaseRef = db.ref(`volumeViews/${volume}`);
    // defaultDatabaseRef.on("value", snapshot => {
    //   this.setState({ viewcount: snapshot.val() || 1 });
    // });
    defaultDatabaseRef.transaction(currentViews => (currentViews || 0) + 1);
    defaultDatabaseRef.on("value", snapshot => {
      this.setState({ viewcount: snapshot.val() });
    });
  }

  render() {
    const { viewcount } = this.state;
    return <span>{viewcount}</span>;
  }
}

ViewCounter.propTypes = { volume: PropTypes.number.isRequired };

export default ViewCounter;
