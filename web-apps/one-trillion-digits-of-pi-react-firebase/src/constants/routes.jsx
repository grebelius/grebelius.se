export const LIBRARY_ROOT = '/';
export const LIBRARY_VIEW = '/';
export const SINGLE_VOLUME_VIEW = '/volume/:volume([1-9]|[1-9][0-9]|[1-9][0-9][0-9]|[1-9][0-9][0-9][0-9]|[1-9][0-9][0-9][0-9][0-9]|[1-9][0-9][0-9][0-9][0-9][0-9]|[1-9][0-9][0-9][0-9][0-9][0-9]|1000000)';
