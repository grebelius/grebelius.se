#!/usr/bin/env bash

echo "Joels build script"

rm -rf .staging
mkdir .staging

# Bygg Hugo och kopiera till staging
cd hugo-static-site
hugo
cp -r public/ ../.staging
cd ..

# Radera filen i Hugo-sitens library-mapp
mv .staging/library/one-trillion-digits-of-pi/index.html .staging/library/one-trillion-digits-of-pi/index2.html

# Bygg React och kopiera till .staging/library
cd web-apps/one-trillion-digits-of-pi-react-firebase
npm run build
cp -r build/ ../../.staging/library/one-trillion-digits-of-pi
cd ..

# rm .staging/library/service-worker.js .staging/library/asset-manifest.json

if [[ "$1" == "deploy" ]]; then
  firebase deploy
else
  firebase serve
fi
