import createCard from "../helpers/createCard";

const createElements = data => {
	return new Promise((resolve, reject) => {
		let el = {
			single: [],
			double: [],
			cardstore: {
				single: new Array(data.pages + 1).fill(null),
				double: new Array(Math.ceil(data.pages / 2) + 1).fill(null),
			},
			card: function(num, type) {
				if (num < 1) return;
				if (num > this.cardstore[type].length) return;
				if (!this.cardstore[type][num]) {
					this.cardstore[type][num] = createCard(num, type);
				}
				return this.cardstore[type][num];
			},
		};

		el.card(1, "single");
		el.card(1, "double");
		el.card(2, "single");
		el.card(2, "double");

		// Double page layout
		el.double.flipbook = document.createElement("div");
		el.double.scenewrapper = document.createElement("div");
		el.double.scene = document.createElement("div");
		el.double.flipscene = document.createElement("div");

		el.double.flipbook.className = "flipbook double";
		el.double.scenewrapper.className = "scenewrapper double";
		el.double.scene.className = "scene double";
		el.double.flipscene.className = "flipscene double";

		el.double.flipbook.style = `--bookWidth:${data.size.px.width};--bookHeight:${data.size.px.height};`;

		// Single page layout
		el.single.flipbook = document.createElement("div");
		el.single.scenewrapper = document.createElement("div");
		el.single.scene = document.createElement("div");
		el.single.flipscene = document.createElement("div");

		el.single.flipbook.className = "flipbook single";
		el.single.scenewrapper.className = "scenewrapper single";
		el.single.scene.className = "scene single";
		el.single.flipscene.className = "flipscene single";

		el.single.flipbook.style = `--bookWidth:${data.size.px.width};--bookHeight:${data.size.px.height};`;

		if (el) {
			resolve(el);
		} else {
			reject(new Error("Couldn't create elements for the flipbook"));
		}
	});
};

export default createElements;
