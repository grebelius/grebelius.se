const createLayout = (rootElement, elements, layoutType) => {
	return new Promise((resolve, reject) => {
		// Empty every element
		elements[layoutType].forEach(el => {
			while (el.firstChild) el.removeChild(el.firstChild);
		});

		// Assign elements to local constants
		const flipbook = elements[layoutType].flipbook;
		const scenewrapper = elements[layoutType].scenewrapper;
		const scene = elements[layoutType].scene;
		const flipscene = elements[layoutType].flipscene;

		// Combine elements
		scene.append(elements.card(2, layoutType), elements.card(1, layoutType));
		scenewrapper.append(scene, flipscene);
		flipbook.appendChild(scenewrapper);

		// Replace old flipbook with new flipbook in rootElement
		rootElement.replaceChild(flipbook, rootElement.firstChild);

		// If too many elements in rootElement, clear it and fill again
		if (rootElement.childElementCount != 1) {
			while (rootElement.firstChild) rootElement.removeChild(rootElement.firstChild);
			rootElement.appendChild(flipbook);
		}

		if (rootElement.firstChild === flipbook && rootElement.childElementCount === 1) {
			resolve();
		} else {
			reject(new Error("Couldn't create layout"));
		}
	});
};

export default createLayout;
