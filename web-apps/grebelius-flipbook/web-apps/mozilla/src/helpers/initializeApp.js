import createElements from "../helpers/createElements";

const initializeApp = async data => {
	let elements = {};

	elements = await createElements(data);

	return new Promise((resolve, reject) => {
		if (elements) {
			resolve(elements);
		} else {
			reject(new Error("Couldn't initialize the flipbook"));
		}
	});
};

export default initializeApp;
