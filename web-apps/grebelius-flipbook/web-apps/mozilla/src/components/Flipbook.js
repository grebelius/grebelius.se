import initializeApp from "../helpers/initializeApp";
import createLayout from "../helpers/createLayout";

const Flipbook = async (data, rootElement) => {
	let state = {
		currentRightmostCard: 1,
		nextcard: function() {
			if (this.currentRightmostCard < this.totalNumberOfCards[this.layoutType]) {
				// Update counter in state
				this.currentRightmostCard++;
				// Make sure next card is in DOM
				if (this.currentRightmostCard + 1 <= this.totalNumberOfCards[this.layoutType]) {
					elements[this.layoutType].scene.append(elements.card(this.currentRightmostCard + 1, this.layoutType));
				}
			}
		},
		previouscard: function() {
			if (this.currentRightmostCard > 1) {
				// Update counter in state
				this.currentRightmostCard--;
				// Make sure previous card is in DOM
				if (this.currentRightmostCard - 2 >= 1) {
					elements[this.layoutType].scene.append(elements.card(this.currentRightmostCard - 1, this.layoutType));
					elements[this.layoutType].scene.append(elements.card(this.currentRightmostCard - 2, this.layoutType));
				}
			}
		},
		layoutType: rootElement.offsetWidth > data.singlePageBreakpointInPixels ? "double" : "single",
		totalNumberOfCards: {
			single: data.pages,
			double: Math.ceil(data.pages / 2),
		},
	};

	// INITIALIZE APP
	const elements = await initializeApp(data);
	await createLayout(rootElement, elements, state.layoutType);

	// --------------------------------------------------------------
	// VARIABLES FOR DRAGGING, CLICKING AND SWIPING
	let isDown = false;
	let startX, startRangeX, startFromCenter, cardBeingFlipped;

	// EVENTLISTENER FOR CARDS BEING DRAGGED OR CLICKED OR SWIPED
	elements.double.scenewrapper.addEventListener("mousedown", handleMouseDown);
	rootElement.addEventListener("mouseup", handleMouseUp);
	rootElement.addEventListener("mousemove", handleMouseMove);

	// --------------------------------------------------------------

	function handleMouseDown(e) {
		e.preventDefault();
		isDown = true;
		const current = state.currentRightmostCard;
		const type = state.layoutType;
		const scenewrapper = elements[state.layoutType].scenewrapper;
		startX = e.pageX - e.currentTarget.offsetLeft;
		startFromCenter = startX - scenewrapper.offsetWidth / 2;
		startRangeX = Math.abs(startFromCenter * 2);
		cardBeingFlipped = startFromCenter <= 0 ? current - 1 : current;

		if (cardBeingFlipped < 1 || cardBeingFlipped > state.totalNumberOfCards[type]) {
			isDown = false;
			cardBeingFlipped = null;
			return;
		}

		const card = elements.card(cardBeingFlipped, type);
		activateCard(card, type);
	}

	function handleMouseMove(e) {
		if (!isDown) return;
		e.preventDefault();
		const type = state.layoutType;
		const scenewrapper = elements[type].scenewrapper;
		const card = elements.card(cardBeingFlipped, type);
		const mouseX = e.pageX - scenewrapper.offsetLeft;
		const fromCenter = mouseX - scenewrapper.offsetWidth / 2;
		const range = fromCenter * 2;

		const degrees = 1.8 * ((range / startRangeX) * 50 - 50);

		if (degrees < -180) {
			card.style.transform = "rotateY(-180deg)";
		} else if (degrees > 0) {
			card.style.transform = "rotateY(0)";
		} else {
			card.style.transform = `rotateY(${degrees}deg)`;
		}
	}

	async function handleMouseUp(e) {
		if (!isDown) return;
		e.preventDefault();
		isDown = false;

		const type = state.layoutType;
		const scenewrapper = elements[type].scenewrapper;
		const scene = elements[type].scene;
		const card = elements.card(cardBeingFlipped, type);
		const finalX = e.pageX - scenewrapper.offsetLeft;
		const finalFromCenter = finalX - scenewrapper.offsetWidth / 2;

		if (finalFromCenter >= 0 && startFromCenter < 0) {
			state.previouscard();
		}
		if (finalFromCenter < 0 && startFromCenter >= 0) {
			state.nextcard();
		}

		deactivateCard(card, scene, state.currentRightmostCard);
	}

	// ****************************************************************
	// PROMISIFY EVERYTHING *******************************************

	const activateCard = card => {
		const flipscene = elements[state.layoutType].flipscene;
		card.classList.add("notransition");
		flipscene.append(card);
	};

	const deactivateCard = (card, scene, current) => {
		card.classList.remove("notransition");
		if (card.dataset.cardnumber < current) {
			// card.classList.add("flipped");
			card.style.transform = "rotateY(-180deg)";
			card.style.zIndex = (1000 - card.dataset.cardnumber) * -1;
		} else {
			// card.classList.remove("flipped");
			card.style.transform = "rotateY(0)";
			card.style.zIndex = 1000 - card.dataset.cardnumber;
		}
		setTimeout(
			() => {
				scene.appendChild(card);
				// card.style.removeProperty("transform");
				[...scene.children].forEach(el => {
					if (Math.abs(el.dataset.cardnumber - current) > 4) el.remove();
				});
			},
			500,
			card,
			scene,
			state.currentRightmostCard
		);
	};

	// **********************************************************************************************************

	// Check for window resize and adapt layout accordingly
	window.addEventListener("resize", async () => {
		const offset = rootElement.offsetWidth - data.singlePageBreakpointInPixels;
		const initialLayoutType = state.layoutType;
		if (state.layoutType === "double" && offset < -10) {
			state.layoutType = "single";
		}
		if (state.layoutType === "single" && offset > 10) {
			state.layoutType = "double";
		}
		if (initialLayoutType != state.layoutType) {
			await createLayout(rootElement, elements, state.layoutType);
		}
	});
	window.addEventListener("mouseout", e => {
		if (e.relatedTarget === null) {
			isDown = false;
		}
	});
};

export default Flipbook;
