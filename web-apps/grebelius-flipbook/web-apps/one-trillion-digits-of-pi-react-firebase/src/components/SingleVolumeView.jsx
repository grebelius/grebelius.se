import React, { Component } from 'react';
import ReactRouterPropTypes from 'react-router-prop-types';

import db from '../firebase';

import PriceCalculator from './PriceCalculator';
import BookViewer from './bookviewer';

class SingleVolumeView extends Component {
  state = {
    viewcount: 1,
  };

  componentDidMount() {
    const { match } = this.props;
    const defaultDatabaseRef = db.ref(`volumeViews/${match.params.volume}`);
    // defaultDatabaseRef.on("value", snapshot => {
    //   this.setState({ viewcount: snapshot.val() || 1 });
    // });
    defaultDatabaseRef.transaction(currentViews => (currentViews || 0) + 1);
    defaultDatabaseRef.on('value', snapshot => {
      this.setState({ viewcount: snapshot.val() });
    });
  }

  render() {
    const { viewcount } = this.state;
    const { match } = this.props;
    return (
      <div>
        <h1>
          Volume number {parseInt(match.params.volume, 10).toLocaleString('en')}, showed {viewcount} times.
        </h1>
        <p>
          <a href={`https://static.grebelius.se/pi/One-Trillion-Digits-of-Pi-${parseInt(match.params.volume, 10)}.pdf`}>
            Download pdf
          </a>
        </p>
        <PriceCalculator viewcount={parseInt(viewcount, 10)} />
        <h2>Read the book</h2>
        <BookViewer volume={parseInt(match.params.volume, 10)} />
      </div>
    );
  }
}

SingleVolumeView.propTypes = {
  match: ReactRouterPropTypes.match.isRequired,
};

export default SingleVolumeView;
