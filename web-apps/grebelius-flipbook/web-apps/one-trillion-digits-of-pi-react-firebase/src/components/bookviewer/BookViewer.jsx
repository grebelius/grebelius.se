import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import ImageOfABookPage from './ImageOfABookPage';

const StyledBookViewer = styled.div`
  display: grid;
  grid-template-columns: 1fr minmax(min-content, max-content) minmax(min-content, max-content) 1fr;
  box-sizing: border-box;
`;

const StyledLeftPage = styled.div`
  grid-column-start: 2;
  grid-column-end: 3;
  background: #efefef;
  box-shadow: 0px 10px 43px -10px rgba(0, 0, 0, 0.5);
`;

const StyledRightPage = styled.div`
  grid-column-start: 3;
  grid-column-end: 4;
  background: #efefef;
  box-shadow: 0px 10px 43px -10px rgba(0, 0, 0, 0.5);
`;

const LeftPage = ({ volume, page }) => {
  if (page > 0) {
    return (
      <StyledLeftPage>
        <ImageOfABookPage volume={volume} page={page} />
      </StyledLeftPage>
    );
  }
  return <div>Början</div>;
};

const RightPage = ({ volume, page }) => {
  if (page > 0) {
    return (
      <StyledRightPage>
        <ImageOfABookPage volume={volume} page={page} />
      </StyledRightPage>
    );
  }
  return <div>Början</div>;
};

class BookViewer extends React.Component {
  state = {
    currentPageNumber: 0,
  };

  render() {
    const { currentPageNumber } = this.state;
    const { volume } = this.props;
    return (
      <StyledBookViewer>
        <LeftPage volume={volume} page={currentPageNumber} />
        <RightPage volume={volume} page={currentPageNumber + 1} />
      </StyledBookViewer>
    );
  }
}

BookViewer.propTypes = { volume: PropTypes.number.isRequired };
LeftPage.propTypes = {
  volume: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
};
RightPage.propTypes = {
  volume: PropTypes.number.isRequired,
  page: PropTypes.number.isRequired,
};

export default BookViewer;
