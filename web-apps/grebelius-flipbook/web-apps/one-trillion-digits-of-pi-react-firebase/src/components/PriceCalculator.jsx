import React from "react";
import PropTypes from "prop-types";

const calculation = function(viewcount) {
  return 314.159 + Math.PI * viewcount;
};

const PriceCalculator = ({ viewcount }) => {
  return (
    <div>
      <em>Price:</em> 314.159 + &pi; * {viewcount} ={" "}
      {calculation(viewcount).toFixed(3)} SEK
    </div>
  );
};

PriceCalculator.propTypes = {
  viewcount: PropTypes.number.isRequired
};

export default PriceCalculator;
