import React from "react";
import { Image, Transformation } from "cloudinary-react";
// import Waypoint from "react-waypoint";

import BookSpineList from "./bookspinelist";

import "./css/LibraryView.css";

const LibraryView = () => (
  <article>
    <header>
      <Image
        cloudName="grebelius"
        publicId="one-trillion-digits-of-pi-stapel-2"
        secure="true"
        dpr="auto"
        responsive
        width="auto"
        quality="auto"
        fetchFormat="auto"
        effect="grayscale"
      >
        <Transformation
          gravity="north"
          width="0.95"
          crop="crop"
          effect="auto_brightness"
        />
      </Image>
      <div className="headlines">
        <div>
          <h1>One Trillion Digits of Pi</h1>
          <h4>
            By <span className="red-text">Lennart Grebelius</span>
          </h4>
          <hr />
          <aside className="pullquote">
            <br />
            “The mathematician’s patterns, like the painter’s or the poet’s,
            must be beautiful; the ideas, like the colours or the words, must
            fit together in a harmonious way. Beauty is the first test: there is
            no permanent place in this world for ugly mathematics.”
            <br />
            <span>—G. H. Hardy (1877–1947)</span>
          </aside>
        </div>
      </div>
    </header>

    <section>
      <p>
        In this work, π has been calculated to one trillion decimal places,
        which must be regarded as a precise approximation. I calculated the
        first 10 million digits on a laptop in my studio. Shigeru Kondo and
        Alexander Yee generously provided me with the remaining 999,990 million.
      </p>

      <aside className="pullquote">
        “Ten decimal places of π are sufficient to give the circumference of the
        earth to a fraction of an inch, and thirty decimal places would give the
        circumference of the visible universe to a quantity imperceptible to the
        most powerful microscope.”
        <br />
        <span>—Simon Newcomb (1835–1909)</span>
      </aside>

      <p>
        Every volume of One Trillion Digits of Pi will be printed on demand as a
        pocket edition limited to only one copy, and priced according to the
        amount of interest the volume did generate at the time of sale. I
        reserve no copyright though. Anyone who wishes to produce millions or
        billions of digits of Pi is free to copy this work.
      </p>
    </section>
    <section>
      <BookSpineList />
    </section>
  </article>
);

export default LibraryView;
