import React from 'react';
import PropTypes from 'prop-types';

import './css/Book.css';

const Book = ({ volume, bookIsBought }) => (
  <div className={`bookSpineDiv${bookIsBought ? ' bookIsBought' : ''}`}>
    <b className="bookSpineTitle">
One Trillion Digits of Pi
    </b>
    <b className="bookSpineSubtitle">
      Volume&nbsp;
      <span className="bookSpineRedTextSpan">
        {parseFloat(volume).toLocaleString('en')}
      </span>
      {' '}
of
      1,000,000
    </b>
    <b className="bookSpineAuthor">
Lennart Grebelius
    </b>
  </div>
);

Book.propTypes = {
  volume: PropTypes.number.isRequired,
  bookIsBought: PropTypes.bool.isRequired,
};

export default Book;
