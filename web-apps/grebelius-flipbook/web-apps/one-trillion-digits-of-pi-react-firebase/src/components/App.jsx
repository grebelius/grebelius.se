import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "./css/App.css";

import * as routes from "../constants/routes";

import LibraryView from "./LibraryView";
import SingleVolumeView from "./SingleVolumeView";
import NoMatch404 from "./NoMatch404";

const App = () => (
  <Router basename="/library/one-trillion-digits-of-pi">
    <Switch>
      <Route exact path={routes.LIBRARY_VIEW} component={LibraryView} />
      <Route
        exact
        path={routes.SINGLE_VOLUME_VIEW}
        component={SingleVolumeView}
      />
      <Route component={() => <NoMatch404 />} />
    </Switch>
  </Router>
);

export default App;
