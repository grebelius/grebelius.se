import firebase from "firebase/app";
import "firebase/database";

const prodConfig = {
  apiKey: "AIzaSyATp-ckHkGb147yFYMm91T2A_dlcTRtaF8",
  authDomain: "pi-react-click-counter-6pp6643.firebaseapp.com",
  databaseURL: "https://pi-react-click-counter-6pp6643.firebaseio.com",
  projectId: "pi-react-click-counter-6pp6643",
  storageBucket: "pi-react-click-counter-6pp6643.appspot.com",
  messagingSenderId: "589307474492"
};

const devConfig = {
  apiKey: "AIzaSyCj_KnU2qViiaZSZc4-56sJiHb1c9DJOj8",
  authDomain: "pi-react-dev-867t9uobqherwfiuv.firebaseapp.com",
  databaseURL: "https://pi-react-dev-867t9uobqherwfiuv.firebaseio.com",
  projectId: "pi-react-dev-867t9uobqherwfiuv",
  storageBucket: "pi-react-dev-867t9uobqherwfiuv.appspot.com",
  messagingSenderId: "938767035813"
};

const config = process.env.NODE_ENV === "production" ? prodConfig : devConfig;

if (!firebase.apps.length) {
  firebase.initializeApp(config);
}

const db = firebase.database();

export default db;
