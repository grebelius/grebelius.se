"use strict";

import "normalize.css";

import "./global.css";
import "./flipper.css";
import "./card.css";

// ready makes sure the document is fully loaded before initializing the Flipper
import ready from "./helpers/ready";

// This data object is unique for every book that is to be flipped.
import piBookData from "./constants";

// This is the flip book function that creates a flip book and inserts it into the DOM
import Flipper from "./components/Flipper";

// And execute!
ready(Flipper(piBookData, document.getElementById("flipper")));
