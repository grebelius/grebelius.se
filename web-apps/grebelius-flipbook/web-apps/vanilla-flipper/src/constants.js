const piBookData = {
  title: "One Trillion Digits of Pi",
  subtitle: function() {
    return `Volume ${this.thisVolumeNumber} of 1,000,000`;
  },
  volumesTotal: 1000000,
  thisVolumeNumber: 2,
  thisVolumeSubtitle: "Volume 1 of 1,000,000",
  pages: 391,
  numberedpages: 386,
  labels(currentCard) {
    console.log(currentCard);
    if (currentCard === 1) {
      return "Volume cover";
    } else if (currentCard === 2) {
      return "ii—iii";
    } else if (currentCard === 196) {
      return "iv—v";
    }
    return (
      currentCard * 2 +
      this.thisVolumeNumber * this.numberedpages -
      this.numberedpages -
      2 +
      "—" +
      (currentCard * 2 - 1)
    );
  },
  size: {
    mm: {
      width: 110,
      height: 178,
      thickness: 24
    },
    px: {
      width: 650,
      height: 1052
    }
  }
};

export default piBookData;
