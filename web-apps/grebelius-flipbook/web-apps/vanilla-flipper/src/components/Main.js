// import Card from "./Card";

const Main = () => {
  const main = document.createElement("main");
  const scene = document.createElement("div");
  const flipscene = document.createElement("div");

  scene.id = "scene";
  flipscene.id = "flipscene";

  main.append(scene, flipscene);

  return main;
};

export default Main;
