const Header = bookData => {
  const header = document.createElement("header");
  const title = document.createElement("h1");
  const subtitle = document.createElement("span");
  title.innerText = bookData.title;
  subtitle.innerText = bookData.subtitle();
  header.append(title, subtitle);

  return header;
};

export default Header;
