import Card from "./Card";
import Header from "./Header";
import Main from "./Main";
import Footer from "./Footer";

const Flipper = (bookData, rootElement) => {
  const totalNumberOfCards = Math.ceil(bookData.pages / 2);
  const cardArray = new Array(totalNumberOfCards + 1).fill(null);

  let currentRightmostCard = 1;
  // let wantedRightmostCard = 1;
  // const preloadedCards = 2;

  // Start downloading first and second image
  // const img1 = new Image();
  // const img2 = new Image();
  // img1.src =
  //   "https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,pg_1,q_auto/one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-1.pdf";
  // img2.src =
  //   "https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,pg_2,q_auto/one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-1.pdf";

  // Create Flipper elements
  const header = Header(bookData);
  const main = Main();
  const footer = Footer();

  const [scene, flipscene] = main.childNodes;
  // eslint-disable-next-line no-unused-vars
  const [leftButton, currentSpreadLabel, rightButton] = footer.childNodes;

  // Create first card and put it into cardArray
  cardArray[1] = Card(1);
  cardArray[2] = Card(2);
  cardArray[3] = Card(3);

  // Append first card to the scene
  scene.append(cardArray[1], cardArray[2], cardArray[3]);

  // Add event listeners
  leftButton.addEventListener("click", handleLeftButton);
  rightButton.addEventListener("click", handleRightButton);

  // Insert elements into the DOM
  rootElement.append(header, main, footer);

  //
  // ------------------------------------------------------------------
  //

  function handleRightButton() {
    const card = cardArray[currentRightmostCard];
    addCard(currentRightmostCard + 1);
    addCard(currentRightmostCard + 2);
    card.animate(scene, flipscene);
    currentRightmostCard++;
  }

  function handleLeftButton() {
    const card = cardArray[currentRightmostCard - 1];
    card.animate(scene, flipscene);
    currentRightmostCard--;
  }

  function addCard(card) {
    if (cardArray[card] === null) cardArray[card] = Card(card);
    if (!main.contains(cardArray[card])) scene.append(cardArray[card]);
  }
};

export default Flipper;
