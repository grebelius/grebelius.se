const Card = cardNumber => {
  const cardInnerHTML = cardNumber => `<div class="cardface-wrapper">
                                            <div class="card__face card__face--front">
                                            front of card ${cardNumber}
                                            </div>
                                            <div class="card__face card__face--back">
                                            back of card ${cardNumber}
                                            </div>
                                            </div>`;

  const newCard = document.createElement("div");
  newCard.className = "card";
  newCard.style.zIndex = 1000 - cardNumber;
  newCard.dataset.cardnumber = cardNumber;
  newCard.innerHTML = cardInnerHTML(cardNumber);

  const flip = () => {
    setTimeout(() => {
      newCard.addEventListener(
        "transitionend",
        scene => {
          flipZIndexSign();
          scene.append(newCard);
        },
        {
          once: true
        }
      );
      newCard.classList.toggle("flipped-left");
    });
  };

  const flipZIndexSign = () =>
    (newCard.style.zIndex = newCard.style.zIndex * -1);

  newCard.animate = (scene, flipscene) => {
    flipscene.append(newCard);
    flip(scene);
  };

  return newCard;
};

export default Card;
