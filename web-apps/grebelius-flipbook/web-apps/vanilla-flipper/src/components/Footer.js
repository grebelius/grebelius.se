const Footer = () => {
  const footer = document.createElement("footer");

  const leftButton = document.createElement("button");
  const rightButton = document.createElement("button");
  const currentSpreadLabel = document.createElement("span");

  leftButton.classList.add("leftButton");
  rightButton.classList.add("rightButton");
  currentSpreadLabel.classList.add("currentSpreadLabel");

  leftButton.innerText = "←";
  rightButton.innerText = "→";
  footer.append(leftButton, currentSpreadLabel, rightButton);

  return footer;
};

export default Footer;
