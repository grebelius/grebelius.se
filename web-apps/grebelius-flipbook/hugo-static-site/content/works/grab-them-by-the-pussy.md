---
title: 'Grab Them by the Pussy'
date: 2018-01-29T10:19:16+01:00
draft: true
work-type: 'Book'
work-year: 2017
work-language: ['en']
series: []
---

_Book. 1 volume, 92 p, ill colour, 30 x 30 cm, paperback English_

A collection of quotes from Donald J. Trump made prior to his presidency.
(Also available as a paperback edition without illustrations).
