---
title: 'One Trillion Digits of Pi'
date: 2018-01-29T10:17:12+01:00
draft: false
work-type: 'Book'
work-year: 2016
work-language: ['en']
series: []
---

<img srcset="
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/Hemsidan/one_trillion_digits_of_pi.jpg 500w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_710/Hemsidan/one_trillion_digits_of_pi.jpg 710w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1000/Hemsidan/one_trillion_digits_of_pi.jpg 1000w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1420/Hemsidan/one_trillion_digits_of_pi.jpg 1420w" src="https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/Hemsidan/one_trillion_digits_of_pi.jpg" />

_Book (Print on Demand of selected volumes). 1 000 000 volumes, 1 000 000 x 398 p, bw, 14 x 21 cm, English._

One million book volumes, each volume consisting of one million digits of the mathematical constant Pi the work comprises a total of 1 trillion digits.
