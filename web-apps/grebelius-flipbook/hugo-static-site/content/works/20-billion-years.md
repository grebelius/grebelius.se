---
title: '20 Billion Years'
date: 2018-01-11T14:22:54+01:00
draft: false
work-type: 'Book'
work-year: 2007
work-language: ['en']
series: []
---

<img srcset="
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/20_billion_years.jpg 500w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_710/v1542016361/Hemsidan/20_billion_years.jpg 710w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1000/v1542016361/Hemsidan/20_billion_years.jpg 1000w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1420/v1542016361/Hemsidan/20_billion_years.jpg 1420w" src="https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/20_billion_years.jpg" />

_Book, 2000 volumes 1 000 000p, 22×21,5 cm, hardcover canvas, English._

Twenty Billion Years consists of 2 000 books, 500 pages each, where every page has 20 000 dots. Each dot symbolizes a year from the creation of the universe to the death of the sun, a perspective over an ungraspable 20 billion years.
