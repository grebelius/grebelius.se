---
title: 'Question Everything'
date: 2018-01-29T10:05:09+01:00
draft: false
work-type: 'Neon sign'
work-year: 2016
work-language: en
series: []
---

<img srcset="
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/question_everything.jpg 500w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_710/v1542016361/Hemsidan/question_everything.jpg 710w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1000/v1542016361/Hemsidan/question_everything.jpg 1000w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1420/v1542016361/Hemsidan/question_everything.jpg 1420w" src="https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/question_everything.jpg" />

_Neon sign. 25 x 110 cm, English._

The expression “Question Everything” in Lennart Grebelius' handwriting transferred into an electric neon sign.
