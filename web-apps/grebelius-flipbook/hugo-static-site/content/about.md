---
title: Who am I?
date: 2018-01-11T10:14:11.000Z
draft: false
slug: about
---

Lennart Grebelius is a Swedish artist based in Gothenburg and London. Since the 1980s his artistry has revolved around the fundamentals of our existence: On the one hand the logical and measurable aspects represented by e.g. time and mathematics, and on the other hand mankind and the limits of our humanity.

The central medium in Grebelius artistic production is the book – as a carrier of memory and testimony, as a mediator of facts but also as an object in itself. Grebelius uses books individually or in groups to gather and organize large quantities of already existing information provided in the form of text, numbers or images. The information in his work is structured and presented in a way which makes the physical design highlight the content. In Man's Inhumanity to Man (1999) for instance, all those who lost their lives in wars during the 18th, 19th and 20th centuries are listed page by page in 43 volumes, each individualrepresented by a cross. The book Mumbai Attack November 2008 (2009), lets us follow the realization of a terrorist attack by means of documentary material. Days at Auschwitz (2015) consists of extracts from the diary of extermination camp physician Dr Johann Kremer. The work is part of a trilogy where Grebelius illustrates the Holocaust solely by quoting persons who describe their daily work to exterminate men, women and children.

In addition to books Grebelius has also worked with e.g. video, large prints and neon signs.

Beside his artistry Grebelius is also a businessman and philanthropist.

Contact: art@grebelius.se