---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
draft: true
work-type: "Book"
work-year:
work-language: [ "en" ]
series: []
---
