---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
date: {{ .Date }}
draft: true
exhibition-place:
exhibition-city:
exhibition-country: "Sweden"
exhibition-year:
exhibition-type:
series: []
---
