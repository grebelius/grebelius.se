import React from "react"

import Layout from "../../../components/layout"
import SEO from "../../../components/seo"

const SelfPortrait = () => (
  <Layout>
    <SEO title="Library: Self Portrait" />
    <h1>Library: Self Portrait</h1>
  </Layout>
)

export default SelfPortrait
