import React from "react"
import { Link } from "gatsby"

import Layout from "../../components/layout"
import SEO from "../../components/seo"

const IndexPage = props => (
  <Layout>
    <SEO title="The Grebelius Libraries" />
    <h1>The Grebelius Libraries</h1>
    <ul>
      <li>
        <Link to={`${props.uri}/one-trillion-digits-of-pi`}>One Trillion Digits of Pi</Link>
      </li>
      <li>
        <Link to={`${props.uri}/self-portrait`}>Self Portrait</Link>
      </li>
    </ul>
  </Layout>
)

export default IndexPage
