import React from "react"
import { Match } from "@reach/router"
import { navigate } from "gatsby"

// import Layout from "../../../components/layout"
import SEO from "../../../components/seo"

import ReflowableBookSpines from "../../../components/pi/ReflowableBookSpines"
import SingleVolumeView from "../../../components/pi/SingleVolumeView"

const libraryBasePath = ""

const IndexPage = props => {
  if (
    props.location.pathname === libraryBasePath ||
    props.location.pathname === libraryBasePath + "/"
  ) {
    return (
      <>
        <SEO title="One Trillion Digits of Pi" />
        <h1>Library: One Trillion Digits of Pi</h1>
        <ReflowableBookSpines uri={props.uri} />
      </>
    )
  } else {
    return (
      <Match path={`${libraryBasePath}/:vol`}>
        {props => {
          if (props.match) {
            const volumeNumber = parseInt(props.match.vol.split("-")[1], 10)
            if (
              typeof volumeNumber === "number" &&
              volumeNumber >= 1 &&
              volumeNumber <= 1000000
            ) {
              return <SingleVolumeView vol={volumeNumber} />
            }
          }
          navigate(libraryBasePath, { replace: true })
        }}
      </Match>
    )
  }
}

export default IndexPage
