import React from "react"

import Image from "./Image"

import bg from "../../images/bg.svg"

const cardFaceInlineStyle = {
  background: `center / contain no-repeat url(${bg}) white`,
}

const Card = ({ vol, cardNumber, position, totalNumberOfCards, bookData }) => {
  const frontPageNumber = cardNumber * 2 - 1
  const backPageNumber = cardNumber * 2

  let cardInlineStyle = {
    transition: `all ${bookData.pageFlipSpeedInMilliseconds}ms`,
  }

  switch (position) {
    case 1:
      cardInlineStyle.zIndex = 10
      cardInlineStyle.transform = "rotateY(-180deg)"
      break
    case 2:
      cardInlineStyle.zIndex = 20
      cardInlineStyle.transform = "rotateY(-180deg)"
      break
    case 3:
      cardInlineStyle.zIndex = 30
      cardInlineStyle.transform = "rotateY(-180deg)"
      break
    case 4:
      cardInlineStyle.transform = "rotateY(-180deg)"
      break
    case 5:
      cardInlineStyle.transform = "rotateY(0)"
      break
    case 6:
      cardInlineStyle.zIndex = 30
      cardInlineStyle.transform = "rotateY(0)"
      break
    case 7:
      cardInlineStyle.zIndex = 20
      cardInlineStyle.transform = "rotateY(0)"
      break
    case 8:
      cardInlineStyle.zIndex = 10
      cardInlineStyle.transform = "rotateY(0)"
      break
    default:
      console.error("Card components need a position parameter")
  }

  let jsx = ""
  if (cardNumber > 0 && cardNumber <= totalNumberOfCards) {
    jsx = (
      <div className="card" style={cardInlineStyle}>
        <div className="card-face" style={cardFaceInlineStyle}>
          <Image url={bookData.pdfUrl(vol, frontPageNumber)} />
          Fram, card number:{cardNumber}, page number: {frontPageNumber}
        </div>
        <div className="card-face back" style={cardFaceInlineStyle}>
          {backPageNumber > bookData.pages.total ? <div className="empty-page-cover-div" /> : (<Image url={bookData.pdfUrl(vol, backPageNumber)} />)}
          Bak, card number: {cardNumber}, page number: {backPageNumber}
        </div>
      </div>
    )
  }

  return <>{jsx}</>
}

export default Card
