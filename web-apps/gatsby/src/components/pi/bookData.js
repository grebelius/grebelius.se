const bookData = {
  title: "One Trillion Digits of Pi",
  get subtitle() {
    return `Volume ${this.volumeNumber} of 1,000,000`
  },
  volumesTotal: 1000000,
  volumeNumber: 2,
  author: "Lennart Grebelius",
  pages: {
    total: 391,
    preface: 3,
    main: 386,
    postface: 2,
  },
  label: function(currentCard) {
    if (currentCard === 1) {
      return "Volume cover"
    } else if (currentCard === 2) {
      return "ii—iii"
    } else if (currentCard === 196) {
      return "iv—v"
    } else if (currentCard > 196 || currentCard < 1) {
      return ""
    }
    return `${currentCard * 2 - 2 - this.pages.preface}—${currentCard * 2 -
      1 -
      this.pages.preface}`
  },
  dimensions: {
    px: { width: 650, height: 1052 },
    mm: { width: 650, height: 1052 },
  },
  imageWidth: 640,
  pdfUrl: function(vol, page) {
    return `https://res.cloudinary.com/grebelius/image/upload/c_scale,dpr_auto,e_sharpen:100,f_auto,pg_${page},q_auto:best,w_${this.imageWidth}/one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-${vol}.pdf`
    // pdfUrl: (vol, page) => `https://res.cloudinary.com/grebelius/image/upload/c_scale,dpr_auto,e_sharpen:100,f_auto,pg_79,q_auto:best,w_${640}/v1539246756/one-trillion-digits-of-pi/One-Trillion-Digits-of-Pi-101.pdf`
  },
  pageFlipSpeedInMilliseconds: 400
}

export default bookData
