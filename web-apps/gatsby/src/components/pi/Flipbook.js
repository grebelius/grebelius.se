import React from "react"

import Card from "./Card"
import bookData from "./bookData"

const Flipbook = ({ vol }) => {
  const [currentRightmostCard, setCurrentRightmostCard] = React.useState(1)

  const totalNumberOfCards = Math.ceil(bookData.pages.total / 2)

  React.useEffect(() => {
    document.addEventListener("keydown", handleKeyup)

    return function cleanup() {
      document.removeEventListener("keydown", handleKeyup)
    }
  })

  const handleKeyup = e => {
    if (e.keyCode === 39) {
      flipForwardsIntent(1)
    } else if (e.keyCode === 37) {
      flipBackwardsIntent(1)
    }
  }

  const flipForwardsIntent = intendedSteps => {
    let diff = totalNumberOfCards + 1 - (currentRightmostCard + intendedSteps)
    if (diff > 0) {
      setCurrentRightmostCard(currentRightmostCard + intendedSteps)
    } else {
      setCurrentRightmostCard(totalNumberOfCards + 1)
    }
  }

  const flipBackwardsIntent = intendedSteps => {
    let diff = currentRightmostCard - intendedSteps
    if (diff > 0) {
      setCurrentRightmostCard(currentRightmostCard - intendedSteps)
    } else {
      setCurrentRightmostCard(1)
    }
  }

  // -------------------------------------------------------------

  const flipbookInlineStyle = {
    "--bookWidth": bookData.dimensions.px.width,
    "--bookHeight": bookData.dimensions.px.height,
  }

  // -------------------------------------------------------------

  return (
    <div className="flipbook" style={flipbookInlineStyle}>
      <div className="scenewrapper">
        <div className="scene">
          <Card
            vol={vol}
            cardNumber={currentRightmostCard - 2}
            key={currentRightmostCard - 2}
            totalNumberOfCards={totalNumberOfCards}
            bookData={bookData}
            position={3}
          />
          <Card
            vol={vol}
            cardNumber={currentRightmostCard + 1}
            key={currentRightmostCard + 1}
            totalNumberOfCards={totalNumberOfCards}
            bookData={bookData}
            position={6}
          />
        </div>
        <div className="flipscene">
          <Card
            vol={vol}
            cardNumber={currentRightmostCard - 1}
            key={currentRightmostCard - 1}
            totalNumberOfCards={totalNumberOfCards}
            bookData={bookData}
            position={4}
          />
          <Card
            vol={vol}
            cardNumber={currentRightmostCard}
            key={currentRightmostCard}
            totalNumberOfCards={totalNumberOfCards}
            bookData={bookData}
            position={5}
          />
        </div>
      </div>
      Label: {bookData.label(currentRightmostCard)}
    </div>
  )
}

export default Flipbook
