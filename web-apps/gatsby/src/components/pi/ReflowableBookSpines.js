import React from "react"
import { Link } from "gatsby"

const ReflowableBookSpines = () => {
  return(
    <ul>
      <li>
        <Link to={`/volume-1-of-1000000`}>Vol 1</Link>
      </li>
      <li>
        <Link to={`/volume-2-of-1000000`}>Vol 2</Link>
      </li>
      </ul>
  )
}

export default ReflowableBookSpines
