import React from "react"

import Flipbook from "./Flipbook"

const SingleVolumeView = ({ vol }) => (
  <>
    <h1>Volume {vol} of 1,000,000</h1>
    <Flipbook vol={vol} />
  </>
)

export default SingleVolumeView
