---
title: Exhibitions
date: 2018-01-11T10:14:11.000Z
draft: false
slug: exhibitions
---

- Göteborgs Auktionsverk, Göteborg, Sweden, 2018.
- Malmö Artist's Book Biennial (MABB), Malmö, Sweden, 2018.
- Borås Museum, Borås, Sweden, 2017.
- Wetterling Gallery, Stockholm, Sweden, 2015.
- Frankfurter Buchmesse, Frankfurt, Germany, 2015.
- The Phatory, New York, 2009.
- Lokstallet, Strömstad, Sweden, 2009.
- The Museum of Modern Art in Borås, Sweden, 2007.
- The Phatory, New York, 2004.
- Swedish Exhibition Agency, 1994.
- The Museum of Natural History, Göteborg, Sweden, 1992.
