---
title: 'Final Meal Requests'
date: 2018-01-29T10:14:51+01:00
draft: false
work-type: 'Book'
work-year: 2005
work-language: ['en']
series: []
---

<img srcset="
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/final_meal_requests.jpg 500w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_710/v1542016361/Hemsidan/final_meal_requests.jpg 710w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1000/v1542016361/Hemsidan/final_meal_requests.jpg 1000w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1420/v1542016361/Hemsidan/final_meal_requests.jpg 1420w" src="https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/final_meal_requests.jpg" />

_Book, 1 volume, 602 p, Hardcover canvas (blue), 34,0 x 29,5 cm, English._

A collection of the last meals of 300 American prisoners sentenced to death, including the name of the prisoner, the date of their execution, and a description of their last meal.
