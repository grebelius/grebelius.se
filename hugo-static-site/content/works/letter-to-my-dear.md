---
title: 'Letter to My Dear'
date: 2018-01-29T10:10:43+01:00
draft: false
work-type: 'Book'
work-year: 2015
work-language: ['en', 'sv']
series: []
---

_Book. 1 volume, 200 p, ills colour, 14 x 21 cm, hb.
Video 15.38 min. English._

Written as a letter to an imaginary child of a future generation, 50 000 years from now, the work pints out the fact that the nuclear waste produced today will be hazardous to man for another 100 000 years. The magnitude of this time span is made visible by means of an apparently infinitely long greeting phrase.

{{< youtube Pf9s68ZZoho >}}
