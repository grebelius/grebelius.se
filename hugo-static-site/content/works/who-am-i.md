---
title: 'Who Am I?'
date: 2018-01-29T10:16:20+01:00
draft: false
work-type: 'Book'
work-year: 2006
work-language: ['en']
series: []
---

<img srcset="
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/who_am_i.jpg 500w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_710/v1542016361/Hemsidan/who_am_i.jpg 710w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1000/v1542016361/Hemsidan/who_am_i.jpg 1000w,
    https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_1420/v1542016361/Hemsidan/who_am_i.jpg 1420w" src="https://res.cloudinary.com/grebelius/image/upload/dpr_auto,f_auto,q_auto/w_500/v1542016361/Hemsidan/who_am_i.jpg" />

_Book, 300 volumes, 210 p, Hardcover canvas (grey), 21,5 x 21,5 x 18 cm, English._

A total of 300 books illustrated with microscopic views of a total of approximately 300 million sperms contained in an average ejaculation.
Exhibited at The Phatory gallery in New York, May-June, 2009.

[http://calendar.artcat.com/exhibits/9276](http://calendar.artcat.com/exhibits/9276)

