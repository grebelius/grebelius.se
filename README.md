# The official homepage of Lennart Grebelius

## How to build

The repo consists of one static site and multiple single-page web apps.

The build script `build.sh`, generates the hugo site and the web apps into their respective build folders. After that it combines them into a single deployable web app and starts a local server.

Use `build.sh deploy` to rebuild and upload to firebase hosting.
